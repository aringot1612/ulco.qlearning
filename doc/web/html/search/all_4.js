var searchData=
[
  ['getheight',['getHeight',['../d0/dd6/classIsland.html#af017bf7f5aa07e0d9ce47ba6f9d99afd',1,'Island']]],
  ['getnextstate',['getNextState',['../d0/d98/classEnvironment.html#ad51fa337a141d57b946d41a1b6984644',1,'Environment']]],
  ['getqsize',['getQSize',['../d0/d98/classEnvironment.html#a2f6d55ae460f6eca455a43867a7b0525',1,'Environment']]],
  ['getqvalue',['getQValue',['../d0/d98/classEnvironment.html#a8aafe1ef2d4e09caf0f911f2dc36d02c',1,'Environment']]],
  ['getrandomaction',['getRandomAction',['../d0/d98/classEnvironment.html#aec2f8dbb74333d5450559d41e455dd74',1,'Environment']]],
  ['getrandommove',['getRandomMove',['../d0/dd6/classIsland.html#a13cad9f685efb811aefda40748f8ed98',1,'Island']]],
  ['getrsize',['getRSize',['../d0/d98/classEnvironment.html#a4b23d35447efbb85367f276e284c0d3f',1,'Environment']]],
  ['getrvalue',['getRValue',['../d0/d98/classEnvironment.html#a93f633cd9ab5af235ae382e578779a3e',1,'Environment']]],
  ['getscore',['getScore',['../d0/dd6/classIsland.html#a49b7426e68ee6f89fdd3d55cdb650c56',1,'Island']]],
  ['getwidth',['getWidth',['../d0/dd6/classIsland.html#a3a175325c75801192a5a356e84edb783',1,'Island']]]
];

#include <chrono>
#include "../utilities/simulation.hpp"

/**
 * @brief Lancement des simulations.
 * 
 * @param argc 
 * @param argv 
 * @return int 
 */
int main(int argc, char **argv) {
	std::srand (unsigned(time(0)));
	// On stocke les sommes de chrono ici.
	long sumChronoEGreedy = 0;
	long sumChronoQLearning = 0;
	long sumChronoMonteCarlo = 0;
	// On stockera les sommes de scores ici.
	int sumEGreedy = 0;
	int sumQLearning = 0;
	int sumMonteCarlo = 0;
	// Paramètres de l'île.
	int islandWidth = 3;
	int islandHeight = 5;
	int islandRhumBottlesNb = 10;
	int maxMoves = 1000;
	//Paramètres du Q-Learning.
	int episodesNb = 200;
	double gamma = 0.9;
	// Nombre de simulations pour affinage.
	double nSims = 1000;

	Simulation simulation(islandHeight, islandWidth, islandRhumBottlesNb);

	std::cout << "Simulation is running, please wait..." << std::endl;
	for(int i = 0 ; i < nSims ; i++){
		// Lancement de l'algorithme E-Greedy.
		auto start = std::chrono::high_resolution_clock::now();
		sumEGreedy += simulation.executeEGreedy(maxMoves);
		sumChronoEGreedy += (long)std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - start).count();
		// Lancement de l'algorithme Q-Learning.
		start = std::chrono::high_resolution_clock::now();
		sumQLearning += simulation.executeQLearning(maxMoves, gamma, episodesNb);
		sumChronoQLearning += (long)std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - start).count();
		// Lancement de l'algorithme Monte Carlo.
		start = std::chrono::high_resolution_clock::now();
		sumMonteCarlo += simulation.executeMonteCarlo(maxMoves);
		sumChronoMonteCarlo += (long)std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - start).count();
	}
	// Affichage des valeurs.
	std::cout << "\n\n\n" << nSims << " xps, environment size " << islandWidth << "x" << islandHeight << ", "<< islandRhumBottlesNb << " local optima and " << maxMoves << " max moves." << std::endl;
	std::cout << "E greedy: " << (sumEGreedy/nSims) << " steps (" << (sumChronoEGreedy/nSims) << " microsecondes per run)." << std::endl;
	std::cout << "Q learning: " << (sumQLearning/nSims) << " steps (" << (sumChronoQLearning/nSims) << " microsecondes per run)." << std::endl;
	std::cout << "MonteCarlo: " << (sumMonteCarlo/nSims) << " steps (" << (sumChronoMonteCarlo/nSims) << " microsecondes per run)." << std::endl;
	std::cout << "\n\nWith Q-Learning properties : gamma = " << gamma << " and episodesNb = " << episodesNb << ".\n\n\n" << std::endl;
	return 0;
};
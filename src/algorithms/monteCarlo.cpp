#include "monteCarlo.hpp"

/**
 * @brief Constructeur de MonteCarlo.
 * 
 * @param _island L'île à utiliser pour l'algorithme Monte Carlo.
 * @param _randomSearch L'instance de Recherche aléatoire à utiliser pour l'algorithme Monte Carlo.
 */
MonteCarlo::MonteCarlo(Island _island, RandomSearch _randomSearch) : island(_island), randomSearch(_randomSearch) {
	moveCount = 0;
	position = std::make_pair(rand() % island.getHeight(), rand() % island.getWidth());
};

/**
 * @brief Permet d'exécuter l'algorithme Monte Carlo.
 * 
 * Si l'algorithme dépasse le nombre de déplacements limite, il sort avec une pénalité.
 * 
 * @param maxMoves Le nombre de déplacements max autorisé pour cet algorithme.
 * 
 * @return int : Le nombre de déplacements utilisé pour trouver le trésor.
 */
int MonteCarlo::compute(int maxMoves){
	// Tant qu'il est possible de réaliser des déplacements et que le mouvement ne renvoie pas le trésor...
	while(move(maxMoves) && moveCount < maxMoves){
		moveCount++;
	}
	// Si la limite de mouvement est respectée...
	if(moveCount < maxMoves)
		return moveCount;
	else // Sinon : pénalité ...
		return maxMoves * 2;
};

/**
 * @brief Exécute un déplacement selon l'algorithme Monte Carlo.
 * 
 * @param maxMoves Le nombre de déplacements max autorisé pour cet algorithme.
 * 
 * @return true : Si le mouvement ne mène pas au trésor.
 * @return false : Si le trésor a été trouvé.
 */
bool MonteCarlo::move(int maxMoves){
	int bestScore = -1;
	int tmpScore = -1;
	std::pair<int, int> tmpPos;
	std::vector<std::pair<int, int>> possiblesPlacements = island.findPlacements(position);
	// Pour chaque déplacement qu'il est possible de réaliser...
	for(unsigned int i = 0 ; i < possiblesPlacements.size() ; i++){
		tmpPos = possiblesPlacements[i];
		// Appel à une Random Search selon une nouvelle position...
		tmpScore = randomSearch.compute(tmpPos, maxMoves);
		// Recherche du meilleur score atteint par une Random Search.
		if(tmpScore > bestScore){
			position = tmpPos;
			bestScore = tmpScore;
		}
	}

	// Si la nouvelle position renvoie le trésor, on retourne false -> l'algorithme s'arrête...
	return (island.getScore(position) != 10);
};
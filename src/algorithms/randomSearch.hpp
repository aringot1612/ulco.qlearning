#include "../utilities/island.hpp"

#pragma once

class RandomSearch{
	private:
		Island island;
		int moveCount;
		std::pair<int, int> position;
		bool move(std::pair<int, int> * pos);
	public:
		RandomSearch(Island _island);
		int compute(std::pair<int, int> initialPos, int maxMoves);
};
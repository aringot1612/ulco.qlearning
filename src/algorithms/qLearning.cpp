#include "qLearning.hpp"

/**
 * @brief Constructeur de Q-Learning.
 * 
 * @param _agent L'agent à utiliser pour l'algorithme Q-Learning.
 * @param _island L'île à utiliser pour l'algorithme Q-Learning.
 * @param episodesNb Le nombre d'épisodes pour le renforcement.
 */
QLearning::QLearning(Agent _agent, Island _island, int episodesNb): agent(_agent), island(_island) {
	moveCount = 0;
	position = std::make_pair(rand() % island.getHeight(), rand() % island.getWidth());
	currentState = island.positionToState(position);
	// Renforcement.
	agent.executeMultipleEpisodes(episodesNb);
};

/**
 * @brief Permet d'exécuter l'algorithme Q-Learning.
 * 
 * Si l'algorithme dépasse le nombre de déplacements limite, il sort avec une pénalité.
 * 
 * @param maxMoves Le nombre de déplacements max autorisé pour cet algorithme.
 * 
 * @return int : Le nombre de déplacements utilisé pour trouver le trésor.
 */
int QLearning::compute(int maxMoves){
	// Tant que le trésor n'a pas été trouvé et qu'il reste des mouvements à effectuer...
	while(island.getScore(island.stateToPosition(currentState)) != 10 && moveCount < maxMoves){
		// Nouveau déplacement.
		move();
		moveCount++;
	}
	// Si la limite de mouvement est respectée...
	if(moveCount < maxMoves)
		return moveCount;
	else // Sinon : pénalité ...
		return maxMoves * 2;
};

/**
 * @brief Exécute un déplacement selon l'algorithme Q-Learning.
 * 
 */
void QLearning::move(){
	currentState = agent.executeBestMove(currentState);
};
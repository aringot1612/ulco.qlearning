#include "randomSearch.hpp"

/**
 * @brief Constructeur de Random Search.
 * 
 * @param _island L'île à utiliser pour l'algorithme Q-Learning.
 */
RandomSearch::RandomSearch(Island _island) : island(_island) {};

/**
 * @brief Permet d'exécuter l'algorithme Random Search.
 * 
 * L'algorithme renvoie le nombre de déplacements max - le nombre de déplacements utilisé.
 * 
 * @param pos La position de départ.
 * @param maxMoves Le nombre de déplacements max autorisé pour cet algorithme.
 * 
 * @return int : Le nombre de déplacements utilisé pour trouver le trésor.
 */
int RandomSearch::compute(std::pair<int, int> pos, int maxMoves){
	moveCount = 0;
	// Tant qu'il reste des mouvements à effectuer et que le trésor n'a pas été trouvé...
	while(move(&pos) && moveCount < maxMoves){
		moveCount++;
	}
	return maxMoves - moveCount;
};

/**
 * @brief Exécute un déplacement selon l'algorithme RandomSearch.
 * 
 * @param pos La position de départ.
 * 
 * @return true : Si le trésor n'a pas été trouvé.
 * @return false : Si le trésor a été trouvé.
 */
bool RandomSearch::move(std::pair<int, int> * pos){
	*pos = island.getRandomMove(island.findPlacements(*pos));
	// Si la nouvelle position renvoie le trésor, on retourne false -> l'algorithme s'arrête...
	return (island.getScore(*pos) != 10);
};
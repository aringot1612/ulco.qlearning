#include "eGreedy.hpp"

/**
 * @brief Constructeur de EGreedy.
 * 
 * @param _island L'île permettant d'utiliser l'algorithme E-Greedy.
 */
EGreedy::EGreedy(Island _island): island(_island){
	moveCount = 0;
	position = std::make_pair(rand() % island.getHeight(), rand() % island.getWidth());
};

/**
 * @brief Permet d'exécuter l'algorithme E-Greedy.
 * 
 * Si l'algorithme dépasse le nombre de déplacements limite, il sort avec une pénalité.
 * 
 * @param maxMoves Le nombre de déplacements max autorisé pour cet algorithme.
 * 
 * @return int : Le nombre de déplacements utilisé pour trouver le trésor.
 */
int EGreedy::compute(int maxMoves){
	// Tant qu'il est possible de réaliser des déplacements et que le mouvement ne renvoie pas le trésor...
	while(moveCount < maxMoves && move()){
		moveCount++;
	}
	// Si la limite de mouvement est respectée...
	if(moveCount < maxMoves)
		return moveCount;
	else // Sinon : pénalité ...
		return maxMoves * 2;
};

/**
 * @brief Exécute un déplacement selon l'algorithme E-Greedy.
 * 
 * @return true : Si le mouvement ne mène pas au trésor.
 * @return false : Si le trésor a été trouvé.
 */
bool EGreedy::move(){
    int bestScore = 0;
	std::pair<int, int> bestMove;
	std::vector<std::pair<int, int>> possiblesPlacements = island.findPlacements(position);
	// Régle de probabilité
	if((float)(rand())/RAND_MAX < 0.9){
		// Recherche du déplacement maximisant...
		for(unsigned int i = 0 ; i < possiblesPlacements.size() ; i++){
			if(island.getScore(possiblesPlacements[i]) >= bestScore){
				bestMove = possiblesPlacements[i];
				bestScore = island.getScore(possiblesPlacements[i]);
			}
		}
		// Récupèration de la meilleure "nouvelle" position.
		position = bestMove;
	}
	else // Récupèration d'une nouvelle position aléatoire.
		position = island.getRandomMove(possiblesPlacements);

	// Si la nouvelle position renvoie le trésor, on retourne false -> l'algorithme s'arrête...
	return (island.getScore(position) != 10);
}
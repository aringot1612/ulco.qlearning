#include "../utilities/island.hpp"

#pragma once

class EGreedy{
	private:
		Island island;
		int moveCount;
		std::pair<int, int> position;
		bool move();
	public:
		EGreedy(Island _island);
		int compute(int maxMoves);
};
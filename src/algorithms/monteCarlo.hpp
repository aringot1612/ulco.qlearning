#include "randomSearch.hpp"

#pragma once

class MonteCarlo{
	private:
		Island island;
		RandomSearch randomSearch;
		int moveCount;
		int bestScore;
		std::pair<int, int> position;
		bool move(int maxMoves);
	public:
		MonteCarlo(Island _island, RandomSearch _randomSearch);
		int compute(int maxMoves);
};
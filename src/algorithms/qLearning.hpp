#include "../utilities/agent.hpp"

#pragma once

class QLearning{
	private:
		Agent agent;
		Island island;
		int currentScore;
		int moveCount;
		std::pair<int, int> position;
		int currentState;
		void move();
	public:
		QLearning(Agent _agent, Island _island, int episodesNb);
		int compute(int maxMoves);
};
#include "../utilities/environment.hpp"
#include "../utilities/agent.hpp"
#include "../algorithms/eGreedy.hpp"
#include "../algorithms/qLearning.hpp"
#include "../algorithms/randomSearch.hpp"
#include "../algorithms/monteCarlo.hpp"

#pragma once

class Simulation{
	private:
		int islandHeight;
		int islandWidth;
		int islandRhumBottlesNb;
		void init();
	public:
		Simulation(int _height, int _width, int _rhumBottlesNb);
		int executeEGreedy(int maxMoves);
		int executeQLearning(int maxMoves, double gamma, int episodesNb);
		int executeMonteCarlo(int maxMoves);
};
#include "simulation.hpp"

/**
 * @brief Constructeur de Simulation.
 * 
 * @param _height Hauteur pour l'île.
 * @param _width Largeur pour l'île.
 * @param _rhumBottlesNb Nombre de bouteilles de rhum pour l'île.
 */
Simulation::Simulation(int _height , int _width, int _rhumBottlesNb) : islandHeight(_height), islandWidth(_width), islandRhumBottlesNb(_rhumBottlesNb){};

/**
 * @brief Exécution de l'algorithme E-Greedy.
 * 
 * @param maxMoves Nombre de mouvement maximum.
 * 
 * @return int : Le nombre de mouvements utilisé.
 */
int Simulation::executeEGreedy(int maxMoves){
	Island island(islandHeight, islandWidth, islandRhumBottlesNb);
	EGreedy eGreedy(island);
	return eGreedy.compute(maxMoves);
};

/**
 * @brief Exécution de l'algorithme Q-Learning.
 * 
 * @param maxMoves Nombre de mouvement maximum.
 * @param gamma Paramètre gamma pour le renforcement du Q-Learning.
 * @param episodesNb Nombre d'épisodes pour le renforcement du Q-Learning.
 * 
 * @return int : Le nombre de mouvements utilisé.
 */
int Simulation::executeQLearning(int maxMoves, double gamma, int episodesNb){
	Island island(islandHeight, islandWidth, islandRhumBottlesNb);
	Environment environment(island);
	Agent agent(gamma, environment);
	QLearning qLearning(agent, island, episodesNb);
	return qLearning.compute(maxMoves);
};

/**
 * @brief Exécution de l'algorithme Monte Carlo.
 * 
 * @param maxMoves Nombre de mouvement maximum.
 * 
 * @return int : Le nombre de mouvements utilisé. 
 */
int Simulation::executeMonteCarlo(int maxMoves){
	Island island(islandHeight, islandWidth, islandRhumBottlesNb);
	RandomSearch randomSearch(island);
	MonteCarlo monteCarlo(island, randomSearch);
	return monteCarlo.compute(maxMoves);
};
#include "agent.hpp"

/**
 * @brief Constructeur de la classe Agent pour l'algorithme Q-Learning.
 * 
 * @param _gamma Le paramètre gamma pour le renforcement de l'algorithme Q-Learning.
 * @param _environment L'instance de classe Environnement contenant les matrices R et Q.
 */
Agent::Agent(double _gamma, Environment _environment) : gamma(_gamma), environment(_environment){};

/**
 * @brief Permet de mettre à jour la matrice Q selon la formule de l'algorithme Q-Learning.
 * 
 * @param state Etat à modifier dans la matrice Q.
 * @param action Action associée.
 * 
 * @return int : Le nouvel état si le trésor n'a pas été trouvé, -1 sinon.
 */
int Agent::updateMatrixQ(int state, int action){
	double maxValue = 0;
	double tmpValue = 0;
	// On récupère le prochain état.
	int nextState = environment.getNextState(state, action);
	// On récupère la valeur de R(s, a).
	int nextStateScore = environment.getRValue(state, action);
	// On vérifie les déplacements possibles associés au prochain état.
	std::vector<int> nextActions = environment.findActions(nextState);
	for(unsigned int i = 0 ; i < nextActions.size() ; i++){
		// Recherche du max selon a' de Q(a', s').
		if(nextActions[i] != -1){
			tmpValue = environment.getQValue(nextState, i);
			if(tmpValue >= maxValue)
				maxValue = tmpValue;
		}
	}
	// Mise à jour de la valeur de Q(s, a).
	environment.setQValue(state, action, nextStateScore + gamma*maxValue);
	// Si le trésor a été trouvé : on retourne -1.
	if(nextStateScore == 10)
		return -1;
	else // Sinon, on renvoie le prochain état.
		return nextState;
};

/**
 * @brief Effectue un épisode.
 * 
 */
void Agent::executeEpisode(){
	// Etat et action aléatoire.
	int state = rand() % environment.getQSize();
	int action = environment.getRandomAction(environment.findActions(state));
	do{
		// Action aléatoire et nouvel état.
		action = environment.getRandomAction(environment.findActions(state));
		state = updateMatrixQ(state, action);
	}while(state != -1); // Tant que le trésor n'a pas été trouvé...
};

/**
 * @brief Effectue n épisodes.
 * 
 * @param n Nombre d'épisodes à effectuer.
 */
void Agent::executeMultipleEpisodes(unsigned int n){
	for(unsigned int i = 0 ; i < n ; i++){
		executeEpisode();
	}
};

/**
 * @brief Permet de retourner la meilleur déplacement d'après la matrice Q.
 * 
 * @param state Etat actuel.
 * 
 * @return int : La meilleure action possible à réaliser.
 */
int Agent::returnBestAction(int state){
	double bestValue = -1;
	double tmpValue = - 1;
	int bestAction = 0 ;
	// Recherche du maximum dans Q(s, i).
	for(unsigned int i = 0 ; i < 4 ; i++){
		tmpValue = environment.getQValue(state, i);
		if(tmpValue >= bestValue){
			bestValue = tmpValue;
			bestAction = i;
		}
	}
	// On retourne la meilleure action réalisable.
	return bestAction;
};

/**
 * @brief Effectue le meilleur mouvement possible en effectuant le meilleur déplacement d'après la matrice Q.
 * 
 * @param state Etat actuel.
 * 
 * @return int : Le nouvel état.
 */
int Agent::executeBestMove(int state){
	return environment.getNextState(state, returnBestAction(state));
};
#include "island.hpp"

#pragma once

class Environment{
	private:
		Island island;
		std::vector<std::vector<double>> QMatrix;
		std::vector<std::vector<int>> RMatrix;
	public:
		Environment(Island _island);
		std::vector<int> findActions(int state);
		int getRandomAction(std::vector<int> possiblesActions);
		void initMatrixQ();
		void initMatrixR();
		void printR();
		void printQ();
		int getNextState(int currentState, int move);
		double getQValue(int i, int j);
		int getRValue(int i, int j);
		void setQValue(int i, int j, double value);
		int findRMatrixValue(int state, int action);
		unsigned int getQSize();
		unsigned int getRSize();
};
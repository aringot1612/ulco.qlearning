#include "environment.hpp"

#pragma once

class Agent{
	private:
		double gamma;
		Environment environment;
	public:
		Agent(double _gamma, Environment _environment);
		int updateMatrixQ(int state, int action);
		void executeEpisode();
		void executeMultipleEpisodes(unsigned int n);
		int executeBestMove(int state);
		int returnBestAction(int state);
};
#include "island.hpp"

/**
 * @brief Constructeur de Island.
 * 
 * @param _height "Hauteur" pour la matrice de l'île.
 * @param _width "Largeur" pour la matrice de l'île.
 * @param _rhumBottlesNb Nombre de bouteilles de rhums à placer.
 */
Island::Island(int _height, int _width, int _rhumBottlesNb) : height(_height), width(_width), rhumBottlesNb(_rhumBottlesNb){
	int maxSize = width*height;
	// Création d'une île au format 1D.
	std::vector<int> tmpIsland(maxSize);
	int bottleCount = 0;
	int vectorIndex = 0;
	bool tresorPlaced = false;
	// Placement des bouteilles de rhum.
	for(int i = 0 ; i < rhumBottlesNb ; i++){
		tmpIsland[rand() % maxSize] = 2;
	}
	// Placement du trésor sur un placement possible.
	do{
		if(tmpIsland[vectorIndex] == 0){
			tmpIsland[vectorIndex] = 10;
			tresorPlaced = true;
		}
		vectorIndex++;
	}while(vectorIndex < (maxSize) && !tresorPlaced);
	// Shuffle de l'île 1D.
	std::random_shuffle(tmpIsland.begin(), tmpIsland.end());
	// Transformation de l'île en tableau 2D.
	island.resize(height, std::vector<int>(width));
	for(int i = 0 ; i < height ; i++)
		for(int j = 0 ; j < width ; j++)
			island[i][j] = tmpIsland[i*width+j];
};

/**
 * @brief Récupèration des placements possibles selon une position donnée.
 * 
 * @param currentPosition Position actuelle dans l'île.
 * 
 * @return std::vector<std::pair<int, int>> : Les nouvelles positions accessibles.
 */
std::vector<std::pair<int, int>> Island::findPlacements(std::pair<int, int> currentPosition){
	std::vector<std::pair<int, int>> possiblesPlacements;

	if(checkBounds(std::make_pair(currentPosition.first - 1, currentPosition.second)))
		possiblesPlacements.push_back(std::make_pair(currentPosition.first - 1,currentPosition.second));

	if(checkBounds(std::make_pair(currentPosition.first, currentPosition.second - 1)))
		possiblesPlacements.push_back(std::make_pair(currentPosition.first,currentPosition.second - 1));
	
	if(checkBounds(std::make_pair(currentPosition.first + 1, currentPosition.second)))
		possiblesPlacements.push_back(std::make_pair(currentPosition.first + 1,currentPosition.second));
		
	if(checkBounds(std::make_pair(currentPosition.first, currentPosition.second + 1)))
		possiblesPlacements.push_back(std::make_pair(currentPosition.first,currentPosition.second + 1));

	return possiblesPlacements;
};

/**
 * @brief Récupèration d'une nouvelle position aléatoire possible à partir d'une liste de positions donnée.
 * 
 * @param possiblesPlacements Liste de positions accessibles.
 * 
 * @return std::pair<int, int> : Nouvelle position aléatoire.
 */
std::pair<int, int> Island::getRandomMove(std::vector<std::pair<int, int>> possiblesPlacements){
	return possiblesPlacements[rand() % possiblesPlacements.size()];
};

/**
 * @brief Vérification des limites de l'île.
 * 
 * @param position Position à vérifier.
 * 
 * @return true : La position est valide : présente dans l'île.
 * @return false : la position est invalide : elle n'est pas située dans l'île.
 */
bool Island::checkBounds(std::pair<int, int> position){
	if(position.first < height && position.first >= 0)
		if(position.second < width && position.second >= 0)
			return true;
	return false;
};

/**
 * @brief Retourne le score associé à une position dans l'île.
 * 
 * @param target Position à vérifier.
 * 
 * @return int : Le score associé à cette position.
 */
int Island::getScore(std::pair<int, int> target){
	return island[target.first][target.second];
};

/**
 * @brief Retourne la largeur de la matrice d'île.
 * 
 * @return int : Largeur de la matrice island.
 */
int Island::getWidth(){
	return width;
};

/**
 * @brief Retourne la hauteur de la matrice d'île.
 * 
 * @return int : Hauteur de la matrice island.
 */
int Island::getHeight(){
	return height;
};

/**
 * @brief Affichage de la matrice d'île.
 * 
 */
void Island::print(){
	std::cout << "\n\nRepresentation de l'ile : \n" << std::endl;
	for(int i = 0 ; i < height ; i++){
		std::cout << std::endl;
		for(int j = 0 ; j < width ; j++){
			std::cout << island[i][j] << " ";
		}
	}
	std::cout << std::endl;
}

/**
 * @brief Convertit un état en une position dans l'île.
 * 
 * @param state Etat à transformer en position.
 * @return std::pair<int, int> : Position associée.
 */
std::pair<int, int> Island::stateToPosition(int state){
	return std::make_pair((int)(state/width), state%width);
};

/**
 * @brief Convertit une position en un état.
 * 
 * @param position Position à transformer en état.
 * @return int : Etat associé.
 */
int Island::positionToState(std::pair<int, int> position){
	return position.first*width+position.second;
};
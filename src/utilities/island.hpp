#include <vector>
#include <algorithm>
#include <random>
#include <utility>
#include <iostream>
#include <stdlib.h>
#include <time.h>

#pragma once

class Island{
	private:
		std::vector<std::vector<int>> island;
		int height;
		int width;
		int rhumBottlesNb;
	public:
		Island(int _height, int _width, int _rhumBottlesNb);
		std::vector<std::pair<int, int>> findPlacements(std::pair<int, int> currentPosition);
		std::pair<int, int> getRandomMove(std::vector<std::pair<int, int>> possiblesPlacements);
		int getScore(std::pair<int, int> target);
		int getWidth();
		int getHeight();
		bool checkBounds(std::pair<int, int> position);
		void print();
		std::pair<int, int> stateToPosition(int state);
		int positionToState(std::pair<int, int> position);
};
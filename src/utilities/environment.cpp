#include "environment.hpp"

/**
 * @brief Constructeur de Environment.
 * 
 * @param _island L'île à utiliser pour créer la matrice R.
 */
Environment::Environment(Island _island) : island(_island){
	// Initialisation des matrices R et Q.
	initMatrixR();
	initMatrixQ();
};

/**
 * @brief Permet de récupèrer les actions possibles (ou non) à partir d'un état donné.
 * 
 * @param state l'état actuel.
 * 
 * @return std::vector<int> : La liste des actions (possibles ou non).
 */
std::vector<int> Environment::findActions(int state){
	std::pair<int, int> pos = island.stateToPosition(state);
	std::vector<int> possiblesPlacements(4, -1);
	// Vérification : NORD.
	if(island.checkBounds(std::make_pair(pos.first - 1, pos.second)))
		possiblesPlacements.at(0) = 0;
	// Vérification : SUD.
	if(island.checkBounds(std::make_pair(pos.first + 1, pos.second)))
		possiblesPlacements.at(1) = 1;
	// Vérification : EST.
	if(island.checkBounds(std::make_pair(pos.first, pos.second + 1)))
		possiblesPlacements.at(2) = 2;
	// Vérification : OUEST.
	if(island.checkBounds(std::make_pair(pos.first, pos.second - 1)))
		possiblesPlacements.at(3) = 3;
	return possiblesPlacements;
};

/**
 * @brief Retourne une action possible aléatoire.
 * 
 * @param possiblesActions Liste des actions à portée.
 * 
 * @return int : Une action aléatoire.
 */
int Environment::getRandomAction(std::vector<int> possiblesActions){
	std::vector<int> valideActions;
	for(unsigned int i = 0 ; i < 4 ; i++){
		if(possiblesActions[i] != (-1))
			valideActions.push_back(i);
	}
	return valideActions[rand() % valideActions.size()];
};

/**
 * @brief Initialisation de la matrice Q à 0.
 * 
 */
void Environment::initMatrixQ(){
	QMatrix.resize(island.getWidth()*island.getHeight(), std::vector<double>(4, 0));
};

/**
 * @brief Initialisation de la matrice R.
 * 
 */
void Environment::initMatrixR(){
	RMatrix.resize(island.getWidth()*island.getHeight(), std::vector<int>(4, -1));
	std::vector<int> moves;
	for(unsigned int i = 0 ; i < RMatrix.size() ; i++){
		moves = findActions(i);
		for(unsigned int j = 0 ; j < moves.size() ; j++){
			if(moves[j] != -1)
				RMatrix[i][j] = findRMatrixValue(i, j);
		}
	}
};

/**
 * @brief Affichage de la matrice R.
 * 
 */
void Environment::printR(){
	std::cout << "\n\nRepresentation de la matrice etat <-> action : \n" << std::endl;
	for(unsigned int i = 0 ; i < RMatrix.size() ; i++){
		std::cout << std::endl;
		for(unsigned int j = 0 ; j < 4 ; j++){
			std::cout << RMatrix[i][j] << " ";
		}
	}
}

/**
 * @brief Affichage de la matrice Q.
 * 
 */
void Environment::printQ(){
	std::cout << "\n\nRepresentation de la matrice Q : \n" << std::endl;
	for(unsigned int i = 0 ; i < QMatrix.size() ; i++){
		std::cout << std::endl;
		for(unsigned int j = 0 ; j < 4 ; j++){
			std::cout << QMatrix[i][j] << " ";
		}
	}
}

/**
 * @brief Récupèration du prochain état atteint depuis un état initial et une direction.
 * 
 * @param currentState Etat initial.
 * @param move Action à réaliser.
 * 
 * @return int : Le nouvel état.
 */
int Environment::getNextState(int currentState, int move){
	switch(move){
		case 0:
			return currentState - island.getWidth();
		case 1:
			return currentState + island.getWidth();
		case 2:
			return currentState + 1;
		case 3:
			return currentState - 1;
		default:
			return -1;
	}
};

/**
 * @brief Retourne la valeur de la matrice Q aux coordonnées i, j.
 * 
 * @param i Coordonnée en i dans la matrice Q.
 * @param j Coordonnée en j dans la matruce Q.
 * 
 * @return double : Valeur dans Q(i, j).
 */
double Environment::getQValue(int i, int j){
	return QMatrix[i][j];
};

/**
 * @brief Retourne la valeur de la matrice R aux coordonnées i, j.
 * 
 * @param i Coordonnée en i dans la matrice R.
 * @param j Coordonnée en j dans la matruce R.
 * 
 * @return double : Valeur dans R(i, j).
 */
int Environment::getRValue(int i, int j){
	return RMatrix[i][j];
}

/**
 * @brief Permet de modifier une valeur dans Q(i, j).
 * 
 * @param i Coordonnée en i dans la matrice Q.
 * @param j Coordonnée en j dans la matrice Q.
 * @param value Nouvelle valeur pour Q(i, j).
 */
void Environment::setQValue(int i, int j, double value){
	QMatrix[i][j] = value;
};

/**
 * @brief Permet de trouver la valeur de R(state, action) depuis l'île.
 * 
 * @param state Etat à utiliser.
 * @param action Action à utiliser.
 * 
 * @return int : Le score à placer dans la matrice R(state, action).
 */
int Environment::findRMatrixValue(int state, int action){
	return island.getScore(island.stateToPosition(getNextState(state, action)));
};

/**
 * @brief Retourne la taille de la matrice Q (nombre d'états).
 * 
 * @return unsigned int : Le nombre d'états.
 */
unsigned int Environment::getQSize(){
	return QMatrix.size();
};

/**
 * @brief Retourne la taille de la matrice R (nombre d'états).
 * 
 * @return unsigned int : Le nombre d'états.
 */
unsigned int Environment::getRSize(){
	return RMatrix.size();
};
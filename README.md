# Apprentissage automatique
## Q Learning

## <br> Développeur

- [Arthur Ringot](https://gitlab.com/aringot1612)

## <br> Résultats de simulations

<br>![simulation île : 3x5 | Q-learning : 200 épisodes](images/simulationResult_3x5__200.png)

<br>![simulation île : 5x7 | Q-learning : 400 épisodes](images/simulationResult_5x7__400.png)

## <br> Comment compiler et exécuter le code

Se placer dans le répertoire racine du projet et taper la commande suivante selon votre OS :

### Linux
	sh ./autorun_linux.sh

### Windows
	sh ./autorun_windows.sh

## <br> Documentation technique

[Accessible ici](https://aringot1612.gitlab.io/ulco.qlearning/html/)